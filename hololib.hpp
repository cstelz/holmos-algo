#ifndef HOLOLIB_HPP_
#define HOLOLIB_HPP_

#include <complex>
#include <cassert>
#include <cstdlib>

typedef std::complex<double> complex;

/*
 * Centers the zero-frequency
 * @param arr complex frequency space array
 * @param square_size x/y-dimension of arr
 */
void fftshift(complex *arr, int square_size);

void fill_random_values(complex *arr, int square_size);

void 

#endif
