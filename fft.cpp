#include <iostream>
#include <complex>
#include <chrono>
#include <cmath>
#include <Magick++.h>
#include <fftw3.h>
#include <omp.h>

using namespace Magick;
using std::cout;
using std::endl;
using std::abs;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using std::chrono::steady_clock;

typedef std::complex<float> complex;

const char *input_filename;
const char *output_filename;
std::complex<float> *image_space, *fourier_space;
Image input, output;
int square_size;
steady_clock::time_point t0, t1, t2;

void fft_shift(int square_size, complex *arr) {
    int half = floor(square_size / (double) 2);
    for(int i=0; i<half; i++) {
	for(int j=0; j<half; j++) {
	    int upper = j + (square_size * i);
	    int lower = upper + (square_size * half) + half;

	    complex cur0 = *(arr + upper);
	    *(arr + upper) = *(arr + lower);
	    *(arr + lower) = cur0;

	    complex cur1 = *(arr + upper + half);
	    *(arr + upper + half) = *(arr + lower - half);
	    *(arr + lower - half) = cur1;
	}
    }
}


int main(int argc, char **argv) {
    if(argc != 3) {
	cout << "Usage: ./fft INPUT OUTPUT" << endl;
	cout << "  INPUT path to grayscale image" << endl;
	cout << "  OUTPUT path to phase image" << endl;
	return -1;
    }
    input_filename = argv[1];
    output_filename = argv[2];

    fftwf_init_threads();
    fftwf_plan_with_nthreads(omp_get_max_threads());
    
    /* Read image from given file */
    t0 = steady_clock::now();
    input.read(input_filename);

    /* Determine largest dimension and fit image to quadratic dim. */
    square_size = input.columns() < input.rows() ? input.rows() : input.columns();
    Geometry padded_size(square_size, square_size);
    padded_size.aspect(true);
    input.extent(padded_size);

    input.modifyImage();
    input.type(TrueColorType);
    PixelPacket *pixel_cache = input.getPixels(0, 0, square_size, square_size);

    image_space = static_cast<complex *>(fftwf_malloc(square_size * square_size * sizeof(complex)));
    fourier_space = static_cast<complex *>(fftwf_malloc(square_size * square_size * sizeof(complex)));

    /* Pixels have been read, fill the image_space array */
    for(int y=0; y<square_size; y++) {
	for(int x=0; x<square_size; x++) {
	    image_space[y*square_size + x].real(pixel_cache[y*square_size + x].red / QuantumRange);
	    image_space[y*square_size + x].imag(0);
	}
    }
    t1 = steady_clock::now();

    /* Now plan the FFT */
    fftwf_plan p;
    p = fftwf_plan_dft_2d(square_size, square_size, // Dimensions
	    reinterpret_cast<fftwf_complex *>(image_space),
	    reinterpret_cast<fftwf_complex *>(fourier_space),
	    FFTW_FORWARD, FFTW_ESTIMATE); // Forward, fast results

    /* And run it */
    fftwf_execute(p);
    t2 = steady_clock::now();

    output = Image(Geometry(square_size, square_size), "black");
    output.modifyImage();
    output.type(TrueColorType);

    Pixels output_cache(output);
    PixelPacket *output_array = output.getPixels(0, 0, square_size, square_size);
    //fft_shift(square_size, fourier_space);

    complex z;
    for(int i=0; i<pow(square_size, 2); i++) {
	z = fourier_space[i];
	output_array[i].red = abs(z) * QuantumRange;
    }

    output_cache.sync();
    output.quantizeColors(1);
    output.write(output_filename);

    fftwf_destroy_plan(p);
    fftwf_free(fourier_space);
    fftwf_free(image_space);
    fftwf_cleanup_threads();

    cout << "Timings in ms: " << endl
	 << "  Buffer prep.\t" << duration_cast<milliseconds>(t1-t0).count() << endl
	 << "  FFT\t" << duration_cast<milliseconds>(t2-t1).count() << endl;

    return 0;
}
