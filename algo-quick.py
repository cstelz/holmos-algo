import cv2
import numpy as np
import matplotlib.pyplot as plt

img = cv2.imread('holmos_raw.png', 0)
cv2.flip(img, 0)
print(img.shape)
img = img / 255.0
w = img.shape[0]
h = img.shape[1]

rect_x = 512 -49
rect_y = 512+15
rect_r = 35

def set_x(x):
    rect_x = x

cv2.namedWindow("Controls", cv2.WINDOW_NORMAL)
cv2.namedWindow("Magspec", cv2.WINDOW_AUTOSIZE)

cv2.createTrackbar("Rect X", "Controls", 0, img.shape[0], set_x)
cv2.createTrackbar("Rect Y", "Controls", 0, img.shape[1], set_x)
cv2.createTrackbar("Rect R", "Controls", 10, int(img.shape[0] / 2), set_x)

while((cv2.waitKey(1) & 0xff) != ord('q')):
    cv2.waitKey(1)
    rect_x = cv2.getTrackbarPos("Rect X", "Controls")
    rect_y = cv2.getTrackbarPos("Rect Y", "Controls")
    rect_r = cv2.getTrackbarPos("Rect R", "Controls")
    ft = np.fft.fftshift(np.fft.fft2(img))
    cv2.waitKey(1)
    magspec = np.log(np.abs(ft))*20
    magspec += abs(np.min(magspec))
    magspec /= np.max(magspec)
    cv2.waitKey(1)

    crop = np.zeros((w, h))
    hh = int(h / 2)
    hw = int(h / 2)
    print(rect_x, rect_y, rect_r)
    cv2.waitKey(1)
    try:
        cropregion = ft[rect_y:(rect_y+rect_r*2),rect_x:(rect_x+rect_r*2)]
        print(cropregion.shape)
        cv2.waitKey(1)
        crop[(hh-rect_r):(hh+rect_r), (hw-rect_r):(hw+rect_r)] = cropregion
    except:
        print("Region wrong...")

    magspec2 = np.log(np.abs(crop))
    magspec2 += abs(np.min(magspec2))
    magspec2 /= np.max(magspec2)

    cv2.waitKey(1)
    crop = np.fft.ifftshift(crop)
    crop = np.fft.ifft2(crop)
    angle = np.angle(crop)
    angle += abs(np.min(angle))
    angle /= np.max(angle)
    print(np.min(angle), np.max(angle))

    cv2.waitKey(1)
    cv2.rectangle(magspec, (rect_x, rect_y), (rect_x+2*rect_r, rect_y+2*rect_r), (255, 0, 0), 2)
    cv2.imshow('Magspec', magspec)
    cv2.imshow('Angle', angle)
    cv2.imshow("Crop", magspec2)
