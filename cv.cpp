#include <iostream>
#include <cmath>
#include <chrono>
#include <complex.h>
#include <cstring>
#include <cassert>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/videoio.hpp>

/* compile with g++ cv.cpp -o cv `pkg-config --cflags --libs opencv` -lfftw3f -ggdb -std=c++11
 * -Wall */

using namespace std;
using namespace cv;

const int square_size = 1024;

void fftshift(Mat& magspec) {
    // Select the four quadrants
    int hw = magspec.rows / 2, hh = magspec.cols / 2;
    Mat tmp;
    Mat top_left(magspec, Rect(0, 0, hw, hh));
    Mat top_right(magspec, Rect(hw, 0, hw, hh));
    Mat bottom_left(magspec, Rect(0, hh, hw, hh));
    Mat bottom_right(magspec, Rect(hw, hh, hw, hh));

    // Swap bottom left and top right quadrant
    top_right.copyTo(tmp);
    bottom_left.copyTo(top_right);
    tmp.copyTo(bottom_left);

    // Swap top left and bottom right quadrant
    top_left.copyTo(tmp);
    bottom_right.copyTo(top_left);
    tmp.copyTo(bottom_right);
}
Mat crop(Mat& src, Rect& roi) {
    assert(src.channels() == 2);
    Mat result = Mat::zeros(src.rows, src.cols, CV_32FC3);
    int left = 0.5 * src.cols - 0.5 * roi.width;
    int top = 0.5 * src.rows - 0.5 * roi.height;

    for(int y=0; y<roi.height; y++) {
	for(int x=0; x<roi.width; x++) {
	    result.at<float>(left+x, top+y) = src.at<float>(roi.x+x, roi.y+y);
	}
    }
    return result;
}

int main(int argc, char **argv) {
    double max;
    int rect_x = 0, rect_y =0, rect_radius = 10;
    chrono::steady_clock::time_point t0, t1;
    t0 = chrono::steady_clock::now();
    Mat img(square_size, square_size, CV_32F);
    Mat in = imread("holmos_raw.png", IMREAD_GRAYSCALE);
    minMaxLoc(in, 0, &max);
    in.convertTo(img, CV_32F, 1/max);
    assert(img.cols == square_size);
    assert(img.rows == square_size);

    Mat planes[] = {img, Mat::zeros(square_size, square_size, CV_32F)};
    Mat complexI;
    merge(planes, 2, complexI);
    dft(complexI, complexI);
    fftshift(complexI);
    split(complexI, planes);
    magnitude(planes[0], planes[1], planes[0]);
    Mat magI = planes[0];
    magI += Scalar::all(1);
    log(magI, magI);

    normalize(magI, magI, 0, 1, CV_MINMAX);

    t1 = chrono::steady_clock::now();

    float secs = (t1 - t0).count();
    cout << "Took " << secs << " s" << endl;

    namedWindow("settings");
    createTrackbar("window x", "settings", &rect_x, square_size);
    createTrackbar("window y", "settings", &rect_y, square_size);
    createTrackbar("window r", "settings", &rect_radius, square_size * 0.5);
    Mat display, cropped_fft, phase(square_size, square_size, CV_32F);
    Rect src_roi, dst_roi;
    Mat crop_planes[] = {Mat(square_size, square_size, CV_32F),
	Mat(square_size, square_size, CV_32F)};
    /*Mat display;
    Mat crop_rect, cropped_fft;
    Mat crop_planes[] = {Mat::zeros(square_size, square_size, CV_32F), Mat::zeros(square_size, square_size, CV_32F)};*/
    while((waitKey(1) & 0xff) != 'q') {
	cropped_fft = Mat::zeros(square_size, square_size, CV_32FC2);
	src_roi.x = rect_x;
	src_roi.y = rect_y;
	src_roi.width = rect_radius * 2;
	src_roi.height = rect_radius * 2;
	dst_roi.x = square_size * 0.5 - rect_radius;
	dst_roi.y = square_size * 0.5 - rect_radius;
	dst_roi.width = rect_radius * 2;
	dst_roi.height = rect_radius * 2;
	magI.copyTo(display);
	rectangle(display, src_roi, 1.0f, 1);
	cout << magI.type() << " " << cropped_fft.type() << endl;
	complexI(src_roi).copyTo(cropped_fft(dst_roi));
	fftshift(cropped_fft);
	dft(cropped_fft, cropped_fft, DFT_INVERSE);
	split(cropped_fft, crop_planes);
	for(int y=0; y<square_size; y++) {
	    for(int x=0; x<square_size; x++) {
		phase.at<float>(x,y) = atan(crop_planes[1].at<float>(x,y) / crop_planes[0].at<float>(x,y));	
	    }
	}
	fftshift(phase);
	/*
	magnitude(crop_planes[0], crop_planes[1], crop_planes[0]);
	crop_planes[0] += Scalar::all(1);
	log(crop_planes[0], crop_planes[0]);
	normalize(crop_planes[0], crop_planes[0], 0, 1, CV_MINMAX);
*/
	/*
	cropped_fft = Mat::zeros(square_size, square_size, CV_32FC3);
	Rect center_roi(static_cast<int>(square_size * 0.5 - rect_radius), static_cast<int>(square_size * 0.5 - rect_radius),
		rect_radius * 2, rect_radius * 2);
	Rect roi(rect_x, rect_y, rect_radius*2, rect_radius*2);
	magI.copyTo(display);
	Mat crop_rect(magI, roi);
	crop_rect.copyTo(cropped_fft(center_roi));
	split(cropped_fft, crop_planes);
*/

	imshow("picture", in);
	//imshow("crop", cropped_fft);
	imshow("spectrum", display);
	imshow("inv crop", phase);
    }

    return 0;
}
