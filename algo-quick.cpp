#include <iostream>
#include <fstream>
#include <complex>
#include <fftw3.h>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

complex<double> *image_space, *fourier_space, *cropped_fourier;

void crop_unshifted_fourier(complex<double> *input, complex<double> *output, int x, int y, int size) {

}

Mat get_magnitude_spectrum(complex<double> *data, int square_size, float fac) {
    Mat magspec(square_size, square_size, CV_32FC1);
    for(int i=0; i<square_size*square_size; i++) {
	magspec.at<float>(i) = abs(data[i]) * fac;
    }
    return magspec;
}

void fftshift(complex<double> *data, int square_size) {
    complex<double> tmp;
    int index1, index2;
    int half = floor(square_size / 2);

    /* Swap upper-left and bottom-right quadrant */
    for(int y=0; y<half; y++) {
	for(int x=0; x<half; x++) {
	    index1 = y * square_size + x;
	    index2 = (y + half) * square_size + x + half;
	    tmp = data[index1];
	    data[index1] = data[index2];
	    data[index2] = tmp;
	}
    }

    /* Swap bottom-left and upper-right quadrant */
    for(int y=half; y<square_size; y++) {
	for(int x=0; x<half; x++) {
	    index1 = y * square_size + x;
	    index2 = (y - half) * square_size + x + half;
	    tmp = data[index1];
	    data[index1] = data[index2];
	    data[index2] = tmp;
	}
    }
}

int main(int argc, char **argv) {
    image_space = new complex<double>[1024*1024];
    cropped_fourier = new complex<double>[1024*1024];
    fourier_space = new complex<double>[1024*1024];

    Mat image = imread("holmos_raw.png", 0);
    Mat magnitude_spectrum(1024, 1024, CV_32F);
    Mat phase_angle(1024, 1024, CV_32FC1);
    for(int i=0; i<1024*1024; i++) {
	image_space[i].real(static_cast<double>(image.at<char>(i)) / 255.0L);
	image_space[i].imag(0.0L);
    }

    cout << image_space[1023*1024+1023].real() << endl;

    /* Perform the FFT with fftw */
    fftw_plan p1, p2;
    p1 = fftw_plan_dft_2d(1024, 1024, 
	    reinterpret_cast<fftw_complex*>(image_space),
	    reinterpret_cast<fftw_complex*>(fourier_space),
	    FFTW_FORWARD, FFTW_ESTIMATE);
    p2 = fftw_plan_dft_2d(1024, 1024,
	    reinterpret_cast<fftw_complex*>(cropped_fourier),
	    reinterpret_cast<fftw_complex*>(image_space),
	    FFTW_BACKWARD, FFTW_ESTIMATE);

    fftw_execute(p1);
    cout << fourier_space[1023*1024+1023] << endl;
    fftshift(fourier_space, 1024);
    memset(cropped_fourier, 0, sizeof(complex<double>)*1024*1024);

    int rect_y = 492;
    int rect_x = 940;
    int rect_w = 70;
    int new_y, new_x;
    for(int y=rect_y; y<(rect_y+rect_w); y++) {
	for(int x=rect_x; x<(rect_x+rect_w); x++) {
	    new_y = 512 + y - rect_y - floor(rect_w * 0.5);
	    new_x = 512 + x - rect_x - floor(rect_w * 0.5);
	    if(y%10==0) cout << "X: " << new_x << " Y: " << new_y << endl;
	    cropped_fourier[new_y*1024+new_x] = fourier_space[y*1024+x];
	}
    }
    imshow("magspec crop", get_magnitude_spectrum(cropped_fourier, 1024, 1/300.0));
    waitKey(0);
    fftshift(cropped_fourier, 1024);
    fftw_execute(p2);

    float angle;
    for(int i=0; i<1024*1024; i++) {
	angle = arg(image_space[i]);
	angle += M_PI;
	angle /= 2.0*M_PI;
	phase_angle.at<float>(i) = angle;
    }
    /* Display a magnitude spectrum */
    magnitude_spectrum = get_magnitude_spectrum(fourier_space, 1024, 1/300.0f);
    rectangle(magnitude_spectrum, Point2f(rect_x, rect_y), Point2f(rect_x+rect_w, rect_y+rect_w), 255);

    flip(phase_angle, phase_angle, 0);

    imshow("magspec", magnitude_spectrum);
    imshow("angle", phase_angle);
    waitKey(0);


    fftw_destroy_plan(p1);
    fftw_destroy_plan(p2);

    return 0;
}
