#include <iostream>
#include <chrono>
#include <cstdlib>
#include "hololib.hpp"
#include <fftw3.h>

using std::cout;
using std::endl;
using std::stoi;

int main(int argc, char **argv) {
    if(argc != 2)
	return -1;
    int square_size = stoi(argv[1]);

    complex *arr = static_cast<complex *>(malloc(square_size * square_size * sizeof(complex)));
    fill_random_values(arr, square_size);

    std::chrono::steady_clock::time_point t0, t1;
    t0 = std::chrono::steady_clock::now();

    fftw_plan p;
    fftw_complex *in;


    t1 = std::chrono::steady_clock::now();
    double msecs = std::chrono::duration_cast<std::chrono::milliseconds>(t1-t0).count();
    cout << "Took " << msecs << "ms" << endl;


    free(arr);
    return 0;
}
