#include "hololib.hpp"


/*
 * Centers the zero-frequency
 * @param arr complex frequency space array
 * @param square_size x/y-dimension of arr
 */
void fftshift(complex *arr, int square_size) {
    int half = floor(square_size / 2);

    complex tmp;

    /* Swaps top-left quadrant with bottom-right */
    #pragma omp parallel for schedule(dynamic,1) collapse(2) numThreads(8)
    for(int y=0; y<half; y++) {
	for(int x=0; x<half; x++) {
	    tmp = arr[y*square_size+x];
	    arr[y*square_size+x] = arr[(y+half)*square_size+half+x];
	    arr[(y+half)*square_size+half+x] = tmp;
	}
    }

    /* Now bottom-left and top-right */
    #pragma omp parallel for schedule(dynamic,1) collapse(2) numThreads(8)
    for(int y=half; y<square_size; y++) {
	for(int x=0; x<half; x++) {
	    tmp = arr[y*square_size+x];
	    arr[y*square_size+x] = arr[(y-half)*square_size+half+x];
	    arr[(y-half)*square_size+half+x] = tmp;
	}
    }
}

void fill_random_values(complex *arr, int square_size) {
    for(int i=0; i<square_size*square_size; i++) {
	arr[i] = complex(rand(), rand());
    }
}
